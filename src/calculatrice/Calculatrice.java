import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Classe permettant d'aficher la fenêtre de la calculatrice basique
 * @author thomas
 *
 */
public class Calculatrice extends JFrame
{
	/**
	 * Panneau contenant l'ensemble des élts GUI
	 */
	protected JPanel container = new JPanel();
	/**
	 * Tableau stockant les éléments à afficher dans la calculatrice
	 */
	String[] tab_string =
		{
				"7", "8", "9", "4", "5", "6", "1", "2", "3", "0", ".", "=", "C", "+", "-","*","/","Bin","Pow","M+","M-","MR","MC"
		};
	/**
	 * Tableau des boutons à afficher
	 */
	JButton[] tab_button = new JButton[tab_string.length];
	/**
	 * Zone correspondant à l'affichage
	 */
	protected JLabel ecran = new JLabel();
	/**
	 * Taille des boutons de "1" à "0", et ".", "=", "C"
	 */
	protected Dimension dim = new Dimension(65, 40);
	/**
	 * Résultat provisoir en cours ...
	 */
	protected double chiffre1;
	/**
	 * "clicOperateur" VRAIE => Opération en cours
	 */
	protected boolean clicOperateur = false;
	/**
	 * "update" VRAIE => première saisie de chiffre
	 */
	protected boolean update = false;
	/**
	 * Dernier opérateur utilisé
	 */
	protected String operateur = "";
	/**
	 * Resultat de la conversion BInaire
	 */
	protected String BinResultConvert;
	/**
	 * Memoire de la calculatrice
	 */
	protected String Memory = "";
	
	/**
	 * Constructeur de la classe "Calculatrice => Sans paramètre(s)
	 */
	public Calculatrice()
	{
		this.setSize(345, 320);
		this.setTitle("Calculatrice HAUTIER");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		//On initialise le conteneur avec tous les composants GUI
		initComposant();
		//On ajoute le conteneur (JPanel à la fenêtre
		this.setContentPane(container);
		this.setVisible(true);
	}

	/**
	 * Construction du panneau avec le contenu de la calculatrice
	 */
	private void initComposant()
	{
		//On définit la police d'écriture à utiliser
		Font police = new Font("Arial", Font.BOLD, 20);
		ecran = new JLabel("0");
		ecran.setFont(police);
		/**
		 * On aligne les informations à droite dans le JLabel
		 */
		ecran.setHorizontalAlignment(JLabel.RIGHT);
		ecran.setPreferredSize(new Dimension(220, 20));
		/**
		 * On défini plusieurs panneaux dans le panneau principal ...
		 */
		JPanel operateur = new JPanel();
		operateur.setLayout(new GridLayout(5,1)); // (55, 225) de base

		JPanel scientifique = new JPanel();
		scientifique.setLayout(new GridLayout(5,1));

		JPanel memory = new JPanel();
		memory.setLayout(new GridLayout(1,4));

		JPanel chiffre = new JPanel();
		chiffre.setLayout(new GridLayout(4,3));

		JPanel panEcran = new JPanel();
		panEcran.setPreferredSize(new Dimension(220, 30));
		
		/**
		 * Definition des couleurs
		 */
		container.setBackground(new Color(5,84,124));
		panEcran.setBackground(new Color(0,71,106));
		ecran.setForeground(new Color(205, 205, 205));
		scientifique.setBackground(new Color(5,84,124));

		//On parcourt le tableau initialisé afin de créer nos boutons :
		for (int i = 0; i < tab_string.length; i++)
		{
			tab_button[i] = new JButton(tab_string[i]);
			tab_button[i].setPreferredSize(dim);
			tab_button[i].setFocusPainted(false);
			switch (i)
			{
			// Pour chaque élément (obj. JButton) ajout
			// d'un listener (écouteur)
			// ...

			/**
			 * égal
			 */
			case 11:
				tab_button[i].setForeground(Color.GRAY);
				tab_button[i].setBackground(new Color(0,71,106));
				tab_button[i].setBorder(BorderFactory.createEmptyBorder());

				tab_button[i].addActionListener(new EgalListener(this));
				chiffre.add(tab_button[i]);
				break;
				/**
				 * Clear
				 */
			case 12:
				tab_button[i].setForeground(Color.GRAY);
				tab_button[i].setBackground(new Color(0,71,106));
				tab_button[i].setBorder(BorderFactory.createEmptyBorder());

				tab_button[i].addActionListener(new ResetListener(this));
				operateur.add(tab_button[i]);
				break;
				/**
				 * addition	
				 */
			case 13 :
				tab_button[i].setForeground(Color.GRAY);
				tab_button[i].setBackground(new Color(0,71,106));
				tab_button[i].setBorder(BorderFactory.createEmptyBorder());

				tab_button[i].addActionListener(new PlusListener(this));
				operateur.add(tab_button[i]);
				break;
				/**
				 * soustraction	
				 */
			case 14 :
				tab_button[i].setForeground(Color.GRAY);
				tab_button[i].setBackground(new Color(0,71,106));
				tab_button[i].setBorder(BorderFactory.createEmptyBorder());

				tab_button[i].addActionListener(new MoinsListener(this));
				operateur.add(tab_button[i]);
				break;
				/**
				 * multiplication
				 */
			case 15 :
				tab_button[i].setForeground(Color.GRAY);
				tab_button[i].setBackground(new Color(0,71,106));
				tab_button[i].setBorder(BorderFactory.createEmptyBorder());

				tab_button[i].addActionListener(new MultiListener(this));
				operateur.add(tab_button[i]);
				break;
				/**
				 * Division
				 */
			case 16 :
				tab_button[i].setForeground(Color.GRAY);
				tab_button[i].setBackground(new Color(0,71,106));
				tab_button[i].setBorder(BorderFactory.createEmptyBorder());

				tab_button[i].addActionListener(new DivListener(this));
				operateur.add(tab_button[i]);
				break;	
				/**
				 * Binaire
				 */
			case 17 :
				tab_button[i].setForeground(Color.GRAY);
				tab_button[i].setBackground(new Color(0,71,106));
				tab_button[i].setBorder(BorderFactory.createEmptyBorder());

				tab_button[i].addActionListener(new BinaryListener(this));
				scientifique.add(tab_button[i]);
				break;
				/**
				 * Puissance
				 */
			case 18 :
				tab_button[i].setForeground(Color.GRAY);
				tab_button[i].setBackground(new Color(0,71,106));
				tab_button[i].setBorder(BorderFactory.createEmptyBorder());

				tab_button[i].addActionListener(new PowListener(this));
				scientifique.add(tab_button[i]);
				break;	
				/**
				 * Memoire Plus
				 */
			case 19 :
				tab_button[i].setForeground(Color.GRAY);
				tab_button[i].setBackground(new Color(0,71,106));
				tab_button[i].setBorder(BorderFactory.createEmptyBorder());

				tab_button[i].addActionListener(new MPlusListener(this));
				memory.add(tab_button[i]);
				break;	
				/**
				 * Memoire Moins
				 */
			case 20 :
				tab_button[i].setForeground(Color.GRAY);
				tab_button[i].setBackground(new Color(0,71,106));
				tab_button[i].setBorder(BorderFactory.createEmptyBorder());

				tab_button[i].addActionListener(new MMoinsListener(this));
				memory.add(tab_button[i]);
				break;	
				/**
				 * Memoire Rappel
				 */
			case 21 :
				tab_button[i].setForeground(Color.GRAY);
				tab_button[i].setBackground(new Color(0,71,106));
				tab_button[i].setBorder(BorderFactory.createEmptyBorder());

				tab_button[i].addActionListener(new MRappelListener(this));
				memory.add(tab_button[i]);
				break;	
				/**
				 * Memoire Clear
				 */
			case 22 :
				tab_button[i].setForeground(Color.GRAY);
				tab_button[i].setBackground(new Color(0,71,106));
				tab_button[i].setBorder(BorderFactory.createEmptyBorder());

				tab_button[i].addActionListener(new MClearListener(this));
				memory.add(tab_button[i]);
				break;	
				/**
				 * Chiffres
				 */
			default:
				chiffre.add(tab_button[i]);
				tab_button[i].setForeground(Color.GRAY);
				tab_button[i].setBackground(new Color(0,71,106));
				tab_button[i].setBorder(BorderFactory.createEmptyBorder());


				tab_button[i].addActionListener(new ChiffreListener(this));
				break;
			}
		}
		/**
		 * initialisation des differents composants visuel
		 */
		panEcran.add(ecran);
		container.add(panEcran, BorderLayout.NORTH);
		container.add(memory, BorderLayout.NORTH);
		container.add(chiffre, BorderLayout.CENTER);
		container.add(operateur, BorderLayout.CENTER);
		container.add(scientifique, BorderLayout.CENTER);
	}

	/**
	 * Méthode permettant d'effectuer un calcul selon l'opérateur sélectionné
	 */
	protected void calcul()
	{
		if(operateur.equals("+"))
		{
			chiffre1 = chiffre1 + Double.valueOf(ecran.getText()).doubleValue();
			ecran.setText(String.valueOf(chiffre1));
		}
		if(operateur.equals("-"))
		{
			chiffre1 = chiffre1 - Double.valueOf(ecran.getText()).doubleValue();
			ecran.setText(String.valueOf(chiffre1));
		}
		if(operateur.equals("/"))
		{
			try
			{
				chiffre1 = chiffre1 / Double.valueOf(ecran.getText()).doubleValue();
				ecran.setText(String.valueOf(chiffre1));
			} 
			catch(ArithmeticException e) 
			{
				ecran.setText("0");
			}
		}
		if(operateur.equals("*"))
		{
			chiffre1 = chiffre1 * Double.valueOf(ecran.getText()).doubleValue();
			ecran.setText(String.valueOf(chiffre1));
		}
		if(operateur.equals("Bin"))
		{
			long Convert = Double.doubleToLongBits(chiffre1);
			BinResultConvert = Long.toBinaryString(Convert);
			ecran.setText(String.valueOf(BinResultConvert));
		}

		if(operateur.equals("Pow"))
		{
			chiffre1 = Math.pow(chiffre1,Double.valueOf(ecran.getText()).doubleValue());
			ecran.setText(String.valueOf(chiffre1));
		}
	}
}
