import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
/**
 * Ecouteurs des chiffres
 * @author thomas
 *
 */
class ChiffreListener implements ActionListener
{
	Calculatrice ObjCalc; 

	public ChiffreListener(Calculatrice c)
	{
		this.ObjCalc = c;
	}

	public void actionPerformed(ActionEvent e)
	{
		System.out.println("[ChiffreListener] Key : " + e.getActionCommand());


		String str = ((JButton)e.getSource()).getText();
		if(ObjCalc.update)
		{
			ObjCalc.update = false;
		}
		else
		{
			if (!ObjCalc.ecran.getText().equals("0"))
				str = ObjCalc.ecran.getText() + str;

		}

		ObjCalc.ecran.setText(str);



	}
}

/**
 * Ecouteur du bouton egal
 * @author thomas
 *
 */
class EgalListener implements ActionListener
{
	Calculatrice ObjCalc;
	public EgalListener(Calculatrice c)
	{
		this.ObjCalc = c;
	}
	public void actionPerformed(ActionEvent arg0)
	{
		System.out.println("[EgalListener] Key : = ");
		//

		ObjCalc.calcul();
		ObjCalc.update = true;
		ObjCalc.clicOperateur = false;

	}
}
/**
 * Ecouteur du bouton +
 * @author thomas
 *
 */
class PlusListener implements ActionListener
{
	Calculatrice ObjCalc;

	public PlusListener(Calculatrice c)
	{
		this.ObjCalc = c;
	}
	public void actionPerformed(ActionEvent arg0)
	{
		System.out.println("[PlusListener] Key : + ");
		//
		if(ObjCalc.clicOperateur){
			ObjCalc.calcul();
			ObjCalc.ecran.setText(String.valueOf(ObjCalc.chiffre1));
		}
		else{
			ObjCalc.chiffre1 = Double.valueOf(ObjCalc.ecran.getText()).doubleValue();
			ObjCalc.clicOperateur = true;
		}
		ObjCalc.operateur = "+";
		ObjCalc.update = true;
	}
}

/**
 * Ecouteur du bouton -
 * @author thomas
 *
 */
class MoinsListener implements ActionListener
{
	Calculatrice ObjCalc;

	public MoinsListener(Calculatrice c)
	{
		this.ObjCalc = c;
	}
	public void actionPerformed(ActionEvent arg0)
	{
		//
		System.out.println("[PlusListener] Key : - ");
		//
		if(ObjCalc.clicOperateur){
			ObjCalc.calcul();
			ObjCalc.ecran.setText(String.valueOf(ObjCalc.chiffre1));
		}
		else{
			ObjCalc.chiffre1 = Double.valueOf(ObjCalc.ecran.getText()).doubleValue();
			ObjCalc.clicOperateur = true;
		}
		ObjCalc.operateur = "-";
		ObjCalc.update = true;
	}
}

/**
 * Ecouteur du bouton /
 * @author thomas
 *
 */
class DivListener implements ActionListener
{
	Calculatrice ObjCalc;

	public DivListener(Calculatrice c)
	{
		this.ObjCalc = c;
	}
	public void actionPerformed(ActionEvent arg0)
	{
		System.out.println("[DivListener] Key : / ");

		//
		if(ObjCalc.clicOperateur){
			ObjCalc.calcul();
			ObjCalc.ecran.setText(String.valueOf(ObjCalc.chiffre1));
		}
		else{
			ObjCalc.chiffre1 = Double.valueOf(ObjCalc.ecran.getText()).doubleValue();
			ObjCalc.clicOperateur = true;
		}
		ObjCalc.operateur = "/";
		ObjCalc.update = true;
	}
}
/**
 * Ecouteur du bouton *
 * @author thomas
 *
 */
class MultiListener implements ActionListener
{
	Calculatrice ObjCalc; 

	public MultiListener(Calculatrice c)
	{
		this.ObjCalc = c;
	}
	public void actionPerformed(ActionEvent arg0)
	{
		System.out.println("[DivListener] Key : * ");
		//
		if(ObjCalc.clicOperateur)
		{
			ObjCalc.calcul();
			ObjCalc.ecran.setText(String.valueOf(ObjCalc.chiffre1));
		}
		else
		{
			ObjCalc.chiffre1 = Double.valueOf(ObjCalc.ecran.getText()).doubleValue();
			ObjCalc.clicOperateur = true;
		}
		ObjCalc.operateur = "*";
		ObjCalc.update = true;
	}
}
/**
 * Ecouteur du bouton Bin
 * @author thomas
 *
 */
class BinaryListener implements ActionListener
{
	Calculatrice ObjCalc; 

	public BinaryListener(Calculatrice c)
	{
		this.ObjCalc = c;
	}
	public void actionPerformed(ActionEvent arg0)
	{
		System.out.println("[BinaryListener] Key : Bin ");
		//
		if(ObjCalc.clicOperateur)
		{
			ObjCalc.calcul();
			ObjCalc.ecran.setText(String.valueOf(ObjCalc.chiffre1));
		}
		else
		{
			ObjCalc.chiffre1 = Double.valueOf(ObjCalc.ecran.getText()).doubleValue();
			

			ObjCalc.clicOperateur = true;
		}
		ObjCalc.operateur = "Bin";
		ObjCalc.update = true;
	}
}
/**
 * Ecouteur du bouton Pow
 * @author thomas
 *
 */
class PowListener implements ActionListener
{
	Calculatrice ObjCalc; 

	public PowListener(Calculatrice c)
	{
		this.ObjCalc = c;
	}
	public void actionPerformed(ActionEvent arg0)
	{
		System.out.println("[PowListener] Key : Pow ");
		//
		if(ObjCalc.clicOperateur)
		{
			ObjCalc.calcul();
			ObjCalc.ecran.setText(String.valueOf(ObjCalc.chiffre1));
		}
		else
		{
			ObjCalc.chiffre1 = Double.valueOf(ObjCalc.ecran.getText()).doubleValue();
			ObjCalc.clicOperateur = true;
		}
		ObjCalc.operateur = "Pow";
		ObjCalc.update = true;
	}
}
/**
 * Ecouteur du bouton M+
 * @author thomas
 *
 */
class MPlusListener implements ActionListener
{
	Calculatrice ObjCalc; 

	public MPlusListener(Calculatrice c)
	{
		this.ObjCalc = c;
	}
	public void actionPerformed(ActionEvent arg0)
	{
		System.out.println("[MPlusListener] Key : M+ ");
		//
		if(ObjCalc.Memory.equals(""))
		{
			ObjCalc.Memory = ObjCalc.ecran.getText();
		}
		else
		{
			ObjCalc.Memory = ""+(Float.parseFloat(ObjCalc.Memory) + Float.parseFloat(ObjCalc.ecran.getText()));
		}
	}
}
/**
 * Ecouteur du bouton M-
 * @author thomas
 *
 */
class MMoinsListener implements ActionListener
{
	Calculatrice ObjCalc; 

	public MMoinsListener(Calculatrice c)
	{
		this.ObjCalc = c;
	}
	public void actionPerformed(ActionEvent arg0)
	{
		System.out.println("[MMoinsListener] Key : M- ");
		//
		if(!ObjCalc.Memory.equals(""))
		{
			ObjCalc.Memory = ""+(Float.parseFloat(ObjCalc.Memory) - Float.parseFloat(ObjCalc.ecran.getText()));
		}
	}
}
/**
 * Ecouteur du bouton MR
 * @author thomas
 *
 */
class MRappelListener implements ActionListener
{
	Calculatrice ObjCalc; 

	public MRappelListener(Calculatrice c)
	{
		this.ObjCalc = c;
	}
	public void actionPerformed(ActionEvent arg0)
	{
		System.out.println("[MRappelListener] Key : MR ");
		//
		ObjCalc.ecran.setText(ObjCalc.Memory);
	}
}
/**
 * Ecouteur du bouton MC
 * @author thomas
 *
 */
class MClearListener implements ActionListener
{
	Calculatrice ObjCalc; 

	public MClearListener(Calculatrice c)
	{
		this.ObjCalc = c;
	}
	public void actionPerformed(ActionEvent arg0)
	{
		System.out.println("[MClearListener] Key : MC ");
		//
		ObjCalc.Memory = "";
		ObjCalc.ecran.setText("0");
	}
}

/**
 * Ecouteur du bouton C (Clear)
 * @author thomas
 *
 */
class ResetListener implements ActionListener
{
	Calculatrice ObjCalc; 

	public ResetListener(Calculatrice c)
	{
		this.ObjCalc = c;
	}
	public void actionPerformed(ActionEvent arg0)
	{
		System.out.println("[DivListener] Key : Clear ");
		//
		ObjCalc.clicOperateur = false;
		ObjCalc.update = true;
		ObjCalc.chiffre1 = 0;
		ObjCalc.operateur = "";
		ObjCalc.ecran.setText("0");
	}
}
